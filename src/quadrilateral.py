import os
import cv2
import numpy as np
import itertools as it
import argparse

POINTS_NUM = 4

def load_image(path):
    if not path or not os.path.isfile(path):
        raise FileNotFoundError('The image path is invalid')

    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

    if image is None or not image.size:
        raise IOError('Image is none')

    return image


def calc_means_2D(mat, window_size):
    """ This function calculates the mean value for each window with a given window_size
    """

    if mat.ndim != 2:
        raise TypeError("This method only supports 2D arrays")

    if window_size > mat.shape[0] or window_size > mat.shape[1]:
        raise ValueError("Invalid window size")

    # Calculate the cumulated sum over the x-axis
    x_cum_sum = mat.cumsum(axis=1)

    # subtract the cumulated sums with the shifted cumulated sums to get the sums for the sliding windows
    x_cum_sum[:, window_size:] = x_cum_sum[:,
                                           window_size:] - x_cum_sum[:, :-window_size]

    # remove the padding
    x_window_sum = x_cum_sum[:, window_size-1:]

    # repeat the steps above with the y axis on sums over the x-axis
    y_cum_sum = x_window_sum.cumsum(axis=0)
    y_cum_sum[window_size:, :] = y_cum_sum[window_size:, :] - \
        y_cum_sum[:-window_size, :]
    y_window_sum = y_cum_sum[window_size-1:, :]

    # calculate the mean value
    window_means = y_window_sum / (window_size*window_size)

    return window_means


def window_overlap(center1, center2, window_size):
    if window_size < 1:
        raise ValueError("Window size cannot be smaller than 0")
    x_overlap = abs(center1[0]-center2[0]) <= (window_size//2)*2
    y_overlap = abs(center1[1]-center2[1]) <= (window_size//2)*2
    return x_overlap and y_overlap


def get_non_overlapping_center_points(mean_mat, num_points, window_size):
    """ This functions finds the n non overlapping patches.
    Therefore it iterates over the patches which are sorted by their bightness in a descending order until it found enough patches.
    The center points will be transformed to the original coordinates
    """

    # flatten the matrix with the mean values and sort the values
    sorted_idx = np.argsort(mean_mat, axis=None).tolist()

    # iterate of the descending mean values and check for overlaps
    point_list = []
    for index in reversed(sorted_idx):

        # since we flattened the matrix we need to recompute the row and col
        row = index // mean_mat.shape[1]
        col = index % mean_mat.shape[1]

        # calculate the center points for each window
        curr_center = (col + window_size//2, row + window_size//2)

        # Check if the current window overlaps with one of the previous selected points
        overlapped = False
        for point in point_list:
            overlapped = window_overlap(curr_center, point, window_size)
            if overlapped:
                break

        if not overlapped:
            # add center to selected points and stop if we have enough points
            point_list.append(curr_center)
            if len(point_list) >= num_points:
                break
    return point_list


def quad_area_shoelace(A, B, C, D):
    # To calculate the area of the quadrilateral we use the Shoelace algorithm.
    # This requires the points to be ordered in a anticlockwise order
    x1 = A[0]
    y1 = A[1]
    x2 = B[0]
    y2 = B[1]
    x3 = C[0]
    y3 = C[1]
    x4 = D[0]
    y4 = D[1]
    # sign +/- if direct/indirect quadrilateral
    return 0.5*(x1*y2+x2*y3+x3*y4+x4*y1-x2*y1-x3*y2-x4*y3-x1*y4)


def ccw(A, B, C):
    # determines whether the points are in counter clockwise order or not
    return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])


def intersect(A, B, C, D):
    # Return true if line segments AB and CD  or BC and AD intersect
    # intersection algorithm found at: https://bryceboe.com/2006/10/23/line-segment-intersection-algorithm/
    intersect_ab_cd = ccw(A, C, D) != ccw(
        B, C, D) and ccw(A, B, C) != ccw(A, B, D)
    intersect_bc_da = ccw(A, B, D) != ccw(
        A, C, D) and ccw(B, C, D) != ccw(A, B, C)

    return (intersect_ab_cd or intersect_bc_da)


def get_best_quadrilateral(points):
    # this method iterates over all permutations of the four points and check if they are a non crossing quadrilateral
    # afterwards it returns the quadrilateral with the highest area
    if len(points) != 4 or len(points) != len(set(points)):
        raise ValueError("Quadrilateral requires 4 distinct points")

    quadriliterals = []
    for quad in it.permutations(points):
        area = quad_area_shoelace(quad[0], quad[1], quad[2], quad[3])
        if not intersect(quad[0], quad[1], quad[2], quad[3]):
            quadriliterals.append((area, quad))

    quadriliterals.sort(key=lambda a: a[0], reverse=True)
    return quadriliterals


def main(image_path, window_size, output_path):

    try:
        image = load_image(args.image)
    except (FileNotFoundError, IOError) as e:
        print("Could not read image", image_path, e)
        return
    if window_size < 1 or window_size * window_size * POINTS_NUM > image.shape[0] * image.shape[1]:
        print('Invalid window size')
        return

    image_window_mean = calc_means_2D(image, window_size)
    center_points = get_non_overlapping_center_points(
        image_window_mean, POINTS_NUM, window_size)
    ordered_quads = get_best_quadrilateral(center_points)
    bgr_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    bgr_image = cv2.fillPoly(bgr_image, np.array(
        [ordered_quads[0][1]]), (0, 0, 255))

    cv2.imwrite(output_path, bgr_image)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A small script to highlight the n patches of a given window size with the highest mean brightness",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--image", help="image path")
    parser.add_argument("-s", "--window_size", default=5,
                        help="window size verbosity")
    parser.add_argument(
        "-o", "--output", default="output/QuadImage.png", help="output path")
    args = parser.parse_args()
    config = vars(args)

    main(args.image, int(args.window_size), args.output)
