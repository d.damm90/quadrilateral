import src.quadrilateral as quad
import numpy as np
import unittest
import itertools as it


class MyTest(unittest.TestCase):
    def test_load_image(self):
        image = quad.load_image("samples/test.png")
        self.assertEqual(image.shape, (1080, 1920))

    def test_load_image_path_None(self):
        self.assertRaises(FileNotFoundError, quad.load_image, None)

    def test_loda_image_path_Wrong(self):
        self.assertRaises(FileNotFoundError, quad.load_image, "WrongPath")

    def test_load_image_path_WrongType(self):
        self.assertRaises(IOError, quad.load_image, "test/__init__.py")

    def test_load_image_path_Is_Folder(self):
        self.assertRaises(IOError, quad.load_image, "test")

    def test_calc_means_2D_unit_mat_odd_window_size(self):
        np_test = np.ones((10, 10))

        mean_mat = quad.calc_means_2D(np_test, 3)
        self.assertTrue(np.all(mean_mat == 1))
        self.assertEqual(mean_mat.shape, (8, 8))

    def test_calc_means_2D(self):
        np_test = np.random.randint(0, 255, (10, 10))

        mean_mat = quad.calc_means_2D(np_test, 3)
        self.assertEqual(np_test[0:3, 0:3].mean(), mean_mat[0, 0])
        self.assertEqual(np_test[7:, 7:].mean(), mean_mat[-1, -1])

    def test_calc_means_2D_unit_mat_even(self):
        np_test = np.ones((10, 10))

        mean_mat = quad.calc_means_2D(np_test, 2)
        self.assertTrue(np.all(mean_mat == 1))

    def test_calc_means_2D_window_size_is_mat_size(self):
        np_test = np.empty((10, 10))

        mean_mat = quad.calc_means_2D(np_test, 10)
        self.assertTrue(np.all(mean_mat == 1))
        self.assertTrue(mean_mat.size == 1)

    def test_calc_means_2D_window_size_to_big(self):
        np_test = np.empty((10, 10))

        self.assertRaises(ValueError, quad.calc_means_2D, np_test, 11)

    def test_calc_means_2D_mat_has_invalid_dims(self):
        np_test = np.empty((10, 10, 10))

        self.assertRaises(TypeError, quad.calc_means_2D, np_test, 2)

    def test_window_overlap_false(self):
        center1 = (2, 2)
        center2 = (5, 5)
        self.assertFalse(quad.window_overlap(center1, center2, 3))

    def test_window_overlap_true(self):
        center1 = (2, 2)
        center2 = (4, 4)
        self.assertTrue(quad.window_overlap(center1, center2, 3))

    def test_window_overlap_only_x(self):
        center1 = (4, 2)
        center2 = (5, 5)
        self.assertFalse(quad.window_overlap(center1, center2, 3))

    def test_window_overlap_only_y(self):
        center1 = (2, 4)
        center2 = (5, 5)
        self.assertFalse(quad.window_overlap(center1, center2, 3))

    def test_window_overlap_zero_window_size(self):
        center1 = (2, 4)
        center2 = (5, 5)
        self.assertRaises(ValueError, quad.window_overlap, center1, center2, 0)

    def test_get_non_overlapping_center_points_no_overlap(self):
        mean_mat = np.zeros((10, 10), dtype=float)

        mean_mat[1, 1] = 220.0
        mean_mat[2, 4] = 200.0
        mean_mat[5, 6] = 180.0
        mean_mat[8, 8] = 160.0

        # the function get_non_overlapping_center_points calculates the center points in the original image
        # mean_mat had a reduced sice due to the padding since it was the mean matrix
        expected_point1 = (2, 2)
        expected_point2 = (5, 3)
        expected_point3 = (7, 6)
        expected_point4 = (9, 9)

        expected = [expected_point1, expected_point2,
                    expected_point3, expected_point4]

        self.assertEqual(quad.get_non_overlapping_center_points(
            mean_mat, 4, 3), expected)

    def test_get_non_overlapping_center_points_with_overlap(self):
        mean_mat = np.zeros((10, 10), dtype=float)

        mean_mat[1, 1] = 220.0
        mean_mat[2, 1] = 220.0      # overlap
        mean_mat[2, 4] = 200.0
        mean_mat[5, 6] = 180.0
        mean_mat[8, 8] = 160.0

        # the function get_non_overlapping_center_points calculates the center points in the original image
        # mean_mat had a reduced sice due to the padding since it was the mean matrix
        expected_point1 = (2, 2)
        expected_point2 = (5, 3)
        expected_point3 = (7, 6)
        expected_point4 = (9, 9)

        expected = [expected_point1, expected_point2,
                    expected_point3, expected_point4]

        self.assertEqual(quad.get_non_overlapping_center_points(
            mean_mat, 4, 3), expected)

    def test_get_non_overlapping_center_points_to_many_points(self):
        mean_mat = np.zeros((10, 10), dtype=float)

        for x in range(0, 10, 3):
            for y in range(0, 10, 3):
                mean_mat[y, x] = 220.0

        self.assertEqual(
            len(quad.get_non_overlapping_center_points(mean_mat, 200, 3)), 16)

    def test_quad_area_shoelace_anti_clock_wise(self):
        point1 = (1, 1)
        point2 = (3, 1)
        point3 = (3, 3)
        point4 = (1, 3)

        self.assertEqual(quad.quad_area_shoelace(
            point1, point2, point3, point4), 4)

    def test_quad_area_shoelace_clock_wise(self):
        point1 = (1, 1)
        point2 = (1, 3)
        point3 = (3, 3)
        point4 = (3, 1)

        self.assertEqual(quad.quad_area_shoelace(
            point1, point2, point3, point4), -4)

    def test_quad_area_shoelace_duplicate_point(self):
        point1 = (1, 1)
        point2 = (3, 1)
        point3 = (2, 3)
        point4 = (1, 1)

        self.assertEqual(quad.quad_area_shoelace(
            point1, point2, point3, point4), 2)

    def test_ccw_true(self):
        point1 = (1, 1)
        point2 = (3, 1)
        point3 = (2, 3)

        self.assertTrue(quad.ccw(point1, point2, point3))

    def test_ccw_false(self):
        point1 = (1, 1)
        point2 = (2, 3)
        point3 = (3, 1)

        self.assertFalse(quad.ccw(point1, point2, point3))

    def test_instersect_true(self):
        point1 = (1, 1)
        point2 = (3, 3)
        point3 = (3, 1)
        point4 = (1, 3)

        self.assertTrue(quad.intersect(point1, point2, point3, point4))

    def test_instersect_false(self):
        point1 = (1, 1)
        point2 = (3, 1)
        point3 = (3, 3)
        point4 = (1, 3)

        self.assertFalse(quad.intersect(point1, point2, point3, point4))

    def test_instersect_duplicate(self):
        point1 = (3, 1)
        point2 = (3, 1)
        point3 = (2, 3)
        point4 = (1, 1)

        self.assertTrue(quad.intersect(point1, point2, point3, point4))

    def test_get_best_quadrilateral_all_point_permutations(self):
        point1 = (1, 1)
        point2 = (1, 3)
        point3 = (3, 3)
        point4 = (3, 1)

        points = [point1, point2, point3, point4]

        expected_area = 4.0

        for points in it.permutations(points):

            quads = quad.get_best_quadrilateral(points)
            self.assertEqual(quads[0][0], expected_area)

    def test_get_best_quadrilateral_duplicate_point(self):
        point1 = (1, 1)
        point2 = (2, 3)
        point3 = (3, 1)
        point4 = (3, 1)

        points = [point1, point2, point3, point4]

        self.assertRaises(ValueError, quad.get_best_quadrilateral, points)

    def test_get_best_quadrilateral_not_enough_points(self):
        point1 = (1, 1)
        point2 = (2, 3)
        point3 = (3, 1)

        points = [point1, point2, point3]

        self.assertRaises(ValueError, quad.get_best_quadrilateral, points)

    def test_get_best_quadrilateral_to_many_points(self):
        point1 = (1, 1)
        point2 = (2, 3)
        point3 = (3, 1)
        point4 = (4, 1)
        point5 = (5, 1)

        points = [point1, point2, point3, point4, point5]

        self.assertRaises(ValueError, quad.get_best_quadrilateral, points)
