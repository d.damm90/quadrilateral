# Quadrilateral

## Executing the script

To run the script on any image use the following cmd:

```
python3 src/quadrilateral.py -i <image_path>
```
To specify the output path of the image please use the cmd:
```
python3 src/quadrilateral.py -i <image_path> -o <output_path>
```

The tests can be run with the cmd:
```
python3 -m unittest test/test_quadrilateral.py  
```

For further help please use:
```
python3 src/quadrilateral.py -h
```
